param(
    [Parameter(Mandatory=$True)][string]$deployScript,
    [Parameter(Mandatory=$True)][string]$bitbucketUser
)

try 
{
    $cloneScript = Join-Path $PSScriptRoot "cloneRepo.ps1"
	$targetDirectory = ".\CD\BuildTools"
	
	Write-Output "Clone script location: $cloneScript"	
    & "$cloneScript" -repoName "cdtools" -targetDirectory "$targetDirectory" -bitbucketUser $bitbucketUser
    
    Write-Output "Executing deploy script."
	$deployScript = Join-Path -Path $targetDirectory -ChildPath "$deployScript"
    & "$deployScript" -doConfigTransform "true"
}
catch 
{
    Write-Output "Something went wrong."
    Write-Output $_.Exception.Message
	exit -1
}