param(
    [Parameter(Mandatory=$True)][string]$repoName,
    [Parameter(Mandatory=$True)][string]$targetDirectory,
    [Parameter(Mandatory=$True)][string]$bitbucketUser,
    [Parameter(Mandatory=$False)][string]$projectName='connectib',
    [Parameter(Mandatory=$False)][string]$branch='master'
)

$repoUrl="https://$bitbucketUser@bitbucket.org/$projectName/$repoName.git"

# Clean out destination folder 
if(Test-Path -Path $targetDirectory)
{
    Remove-Item -Path $targetDirectory -Recurse -Force
}

# Clone from repo to folder 
if($branch -eq 'master')
{
    & git clone --depth 1 $repoUrl $targetDirectory -q | out-null
    Write-Host "Cloned $repoUrl."
}
else
{
    & git clone $repoUrl $targetDirectory -q | out-null
    Write-Host "Cloned $repoUrl."
    
    # switch branch
    $currentDir = Get-Item -Path "."
    Set-Location $targetDirectory
    & git checkout $branch -q | out-null
    Set-Location $currentDir
    Write-Host "Switched to branch '$branch'."
}
