﻿using ConnectLabs.Estate.Access.Contract;
using ConnectLabs.Meeting.Access.Contract;
using ConnectLabs.MIS.Access.Contract;
using ConnectLabs.Presence.Access.Contract;

namespace ConnectLabs.MISIntegration.Manager.Service
{
    public interface IMigrateRoomPresencePolicy
    {
        MISLinkResponse GetMisLinkDetails(MISLinkRequest request);

        RoomsMISResponse GetRoomDetails(RoomsMISRequest request);

        PresenceMISResponse GetRoomPresenceForMIS(PresenceMISRequest request);

        //CalculateUtilizationResponse CalculateRoomUtilization(CalculateUtilizationRequest request);

        MISRoomPresenceLastProcessedDateResponse LastProcessedDate(MISRoomPresenceLastProcessedDateRequest request);

        //RemoveBookingsResponse RemoveRoomBookings(RemoveBookingsRequest request);

        //RemoveUtilizationResponse RemoveRoomUtilization(RemoveUtilizationRequest request);

        //SaveRoomBookingResponse MigrateRoomBookings(SaveRoomBookingRequest request);

        //UpdateUtilizationResponse UpdateRoomUtilization(UpdateUtilizationRequest request);

    }
}
