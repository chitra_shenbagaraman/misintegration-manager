﻿using ConnectLabs.Estate.Access.Contract;
using ConnectLabs.Meeting.Access.Contract;
using ConnectLabs.MIS.Access.Contract;

namespace ConnectLabs.MISIntegration.Manager.Service
{
    public interface IMigrateRoomBookingsPolicy
    {
        MISLinkResponse GetMisLinkDetails(MISLinkRequest request);

        RoomsMISResponse GetRoomDetails(RoomsMISRequest request);

        BookingsMISResponse GetRoomBookingsForMIS(BookingsMISRequest request);

        CalculateUtilizationResponse CalculateRoomUtilization(CalculateUtilizationRequest request);

        MISLastProcessedDateResponse LastProcessedDate(MISLastProcessedDateRequest request);

        RemoveBookingsResponse RemoveRoomBookings(RemoveBookingsRequest request);

        RemoveUtilizationResponse RemoveRoomUtilization(RemoveUtilizationRequest request);

        SaveRoomBookingResponse MigrateRoomBookings(SaveRoomBookingRequest request);

        UpdateUtilizationResponse UpdateRoomUtilization(UpdateUtilizationRequest request);

    }
}
