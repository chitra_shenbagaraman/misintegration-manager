﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectLabs.MISIntegration.Manager.Service.Contract
{
    public interface IMISIntegrationManager
    {
        MigrateResponse Migrate(MigrateRequest request);
    }
}
