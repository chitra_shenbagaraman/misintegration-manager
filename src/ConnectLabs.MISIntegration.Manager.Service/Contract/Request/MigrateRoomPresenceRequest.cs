﻿
using System;
using System.Runtime.Serialization;
using Version = ConnectLabs.Connect.Interface.Trunking.Version;

namespace ConnectLabs.MISIntegration.Manager.Service.Contract
{
    public class MigrateRoomPresenceRequest : MigrateRequest
    {
        [DataMember]
        public DateTime? Date { get; set; }

        [DataMember]
        public string Username { get; set; }

        public MigrateRoomPresenceRequest()
            : base(new Version(1)) { }
    }
}
