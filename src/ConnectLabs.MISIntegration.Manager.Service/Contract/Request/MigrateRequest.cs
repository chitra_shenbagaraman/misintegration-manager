﻿using System.Runtime.Serialization;
using ConnectLabs.Connect.Interface.Trunking;

namespace ConnectLabs.MISIntegration.Manager.Service.Contract
{
    [DataContract(Namespace = "ConnectLabs.MISIntegration.Manager.Contract")]
    [KnownType(typeof(MigrateRoomBookingsRequest))]
    [KnownType(typeof(MigrateRoomPresenceRequest))]
    public class MigrateRequest : Request
    {
        public MigrateRequest(Version version)
            : base(version) { }
    }
}
