﻿using ConnectLabs.Connect.Interface.Trunking;
using System.Runtime.Serialization;

namespace ConnectLabs.MISIntegration.Manager.Service.Contract
{
    [DataContract(Namespace = "ConnectLabs.MISIntegration.Manager.Contract")]
    [KnownType(typeof(MigrateRoomBookingsResponse))]
    [KnownType(typeof(MigrateRoomPresenceResponse))]
    public class MigrateResponse : Response
    {
        public MigrateResponse(Version version)
            : base(version) { }
    }
}
