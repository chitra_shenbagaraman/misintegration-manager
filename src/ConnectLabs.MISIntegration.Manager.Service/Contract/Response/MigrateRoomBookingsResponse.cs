﻿using ConnectLabs.Connect.Interface.Trunking;
using System.Runtime.Serialization;


namespace ConnectLabs.MISIntegration.Manager.Service.Contract
{
    [DataContract(Namespace = "ConnectLabs.MISIntegration.Manager.Contract")]
    public class MigrateRoomBookingsResponse : MigrateResponse
    {
        public MigrateRoomBookingsResponse()
           : base(new Version(1)) { }
    }
}
