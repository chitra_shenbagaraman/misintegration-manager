﻿using ConnectLabs.MIS.Access.Contract;
using ConnectLabs.Connect.Interface.Trunking;
using ConnectLabs.MISIntegration.Manager.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using ConnectLabs.Estate.Access.Contract;
using ConnectLabs.Meeting.Access.Contract;

namespace ConnectLabs.MISIntegration.Manager.Service
{
    [RequestHandler]
    public class MigrateRoomBookingsHandler
    {
        private readonly IMigrateRoomBookingsPolicy _policy;

        public MigrateRoomBookingsHandler(IMigrateRoomBookingsPolicy policy)
        {
            _policy = policy ?? throw new ArgumentNullException(nameof(policy));
        }

        public MigrateRoomBookingsResponse Migrate(MigrateRoomBookingsRequest request)
        {
            var response = new MigrateRoomBookingsResponse();

            var misLinks = _policy.GetMisLinkDetails(new Estate.Access.Contract.MISLinkRequest() { }).MisLinks;

            foreach (var region in misLinks)
            {
                ProcessRegionBookings(region);
            }

            return response;
        }

        private void ProcessRegionBookings(MISLinkItem  misLinkDetail)
        {
            var endDate = DateTime.Now;

            var lastProcessedDate = _policy.LastProcessedDate(new MISLastProcessedDateRequest
            {
                ClientId = misLinkDetail.MisClientId,
                RegionId = misLinkDetail.MisRegionId
            });

            var startDate = lastProcessedDate.LastProcessedDate == DateTime.MinValue ? (DateTime?)null : endDate.AddDays(-3);

            var roomDetails = _policy.GetRoomDetails(new RoomsMISRequest { }).Rooms;

            var meetingsForMis = _policy.GetRoomBookingsForMIS(new Meeting.Access.Contract.BookingsMISRequest
            {
                RoomKeys = roomDetails.Select(r => r.Key).ToList(),
                Start = startDate,
                End = endDate
            }).Bookings.OrderBy(rb => rb.Start);

            if (meetingsForMis.Any())
            {
                MigrateBookingsCalculateAndUpdateUtilization(misLinkDetail, meetingsForMis, startDate, endDate, roomDetails);
             }
            else
            {
                _policy.RemoveRoomUtilization(new RemoveUtilizationRequest
                {
                    ClientId = misLinkDetail.MisClientId,
                    RegionId = misLinkDetail.MisRegionId,
                    From = startDate,
                    To = endDate
                });
            }

        }

        private void MigrateBookingsCalculateAndUpdateUtilization(MISLinkItem misLinkDetail, IOrderedEnumerable<MeetingBookingMIS> meetingsForMis, DateTime? startDate, DateTime endDate, List<RoomMISItem> roomDetails)
        {
            _policy.RemoveRoomBookings(new RemoveBookingsRequest
            {
                ClientId = misLinkDetail.MisClientId,
                RegionId = misLinkDetail.MisRegionId,
                From = meetingsForMis.OrderBy(b => b.Start).FirstOrDefault().Start,
                To = endDate
            });

            SaveRoomBookingRequest srbr = new SaveRoomBookingRequest
            {
                RoomBookings = new List<MIS.Access.Contract.DataObjects.RoomBookingItem>()
            };

            foreach (var item in meetingsForMis)
            {
                srbr.RoomBookings.Add(new MIS.Access.Contract.DataObjects.RoomBookingItem
                {
                    BeginsAt = item.Start,
                    BookingId = int.Parse(item.CalendarEntryKey),
                    BookingRef = roomDetails.SingleOrDefault(r => r.Key.ToString() == item.RoomKey).Name,
                    BookingSource = item.BookingSource,
                    BookingText = item.Description,
                    EndsAt = item.End,
                });
            }

            srbr.ClientId = misLinkDetail.MisClientId;
            srbr.RegionId = misLinkDetail.MisRegionId;

            var savebookings = _policy.MigrateRoomBookings(srbr);

            var earliestDate = meetingsForMis.OrderBy(b => b.Start).FirstOrDefault().Start;
            var latestDate = meetingsForMis.OrderByDescending(b => b.End).FirstOrDefault().End;

            var misCalcResponse = _policy.CalculateRoomUtilization(new CalculateUtilizationRequest
            {
                ClientId = misLinkDetail.MisClientId,
                RegionId = misLinkDetail.MisRegionId,
                From = new DateTime(earliestDate.Year, earliestDate.Month,earliestDate.Day,earliestDate.Hour,0,0),
                To = new DateTime(latestDate.Year, latestDate.Month, latestDate.Day, latestDate.Hour, 0, 0)
            });

            var misUpdateUtilizationResponse = _policy.UpdateRoomUtilization(new UpdateUtilizationRequest
            {
                ClientId = misLinkDetail.MisClientId,
                RegionId = misLinkDetail.MisRegionId,
            });

        }


    }
}
