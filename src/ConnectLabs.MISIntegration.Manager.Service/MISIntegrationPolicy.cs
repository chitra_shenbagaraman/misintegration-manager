﻿using ConnectLabs.Connect.Interface.Trunking;
using ConnectLabs.Estate.Access.Contract;
using ConnectLabs.Meeting.Access.Contract;
using ConnectLabs.MIS.Access.Contract;
using ConnectLabs.Presence.Access.Contract;
using System;

namespace ConnectLabs.MISIntegration.Manager.Service
{
    [Policy]
    public class MISIntegrationPolicy : IMigrateRoomBookingsPolicy, IMigrateRoomPresencePolicy
    {
        private readonly IEstateAccess _estate;
        private readonly IMeetingAccess _meeting;
        private readonly IMISAccess _mis;
        private readonly IPresenceAccess _presence;

        public MISIntegrationPolicy(
            IEstateAccess estate,
            IMeetingAccess meeting,
            IMISAccess mis,
            IPresenceAccess presence
            )
        {
            _estate = estate ?? throw new ArgumentNullException(nameof(estate));
            _meeting = meeting ?? throw new ArgumentNullException(nameof(meeting));
            _mis = mis ?? throw new ArgumentNullException(nameof(mis));
            _presence = presence ?? throw new ArgumentNullException(nameof(presence));
        }

        public CalculateUtilizationResponse CalculateRoomUtilization(CalculateUtilizationRequest request)
        {
            return (CalculateUtilizationResponse)_mis.Maintain(request);
        }

        public MISLinkResponse GetMisLinkDetails(MISLinkRequest request)
        {
            return (MISLinkResponse)_estate.View(request);
        }

        public BookingsMISResponse GetRoomBookingsForMIS(BookingsMISRequest request)
        {
            return (BookingsMISResponse)_meeting.Find(request);
        }

        public RoomsMISResponse GetRoomDetails(RoomsMISRequest request)
        {
            return (RoomsMISResponse)_estate.View(request);
        }

        public MISLastProcessedDateResponse LastProcessedDate(MISLastProcessedDateRequest request)
        {
            return (MISLastProcessedDateResponse)_mis.View(request);
        }

        public RemoveBookingsResponse RemoveRoomBookings(RemoveBookingsRequest request)
        {
            return (RemoveBookingsResponse)_mis.Maintain(request);
        }

        public RemoveUtilizationResponse RemoveRoomUtilization(RemoveUtilizationRequest request)
        {
            return (RemoveUtilizationResponse)_mis.Maintain(request);
        }

        public SaveRoomBookingResponse MigrateRoomBookings(SaveRoomBookingRequest request)
        {
            return (SaveRoomBookingResponse)_mis.Maintain(request);
        }

        public UpdateUtilizationResponse UpdateRoomUtilization(UpdateUtilizationRequest request)
        {
            return (UpdateUtilizationResponse)_mis.Maintain(request);
        }

        public PresenceMISResponse GetRoomPresenceForMIS(PresenceMISRequest request)
        {
            return (PresenceMISResponse)_presence.Find(request);
        }

        public MISRoomPresenceLastProcessedDateResponse LastProcessedDate(MISRoomPresenceLastProcessedDateRequest request)
        {
            return (MISRoomPresenceLastProcessedDateResponse)_mis.View(request);
        }
    }
}
