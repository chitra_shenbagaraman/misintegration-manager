﻿using ConnectLabs.Connect.Interface.Trunking;
using ConnectLabs.Meeting.Access.Contract;

namespace ConnectLabs.Meeting.Access.Proxy
{
    public class MeetingAccess : IMeetingAccess
    {
        private readonly IHttpProxy _router;

        private const string ServiceUrl = @"http://localhost:19081/Meeting.Access/WebApi/api/Meeting/";

        public MeetingAccess(IProxyFactory proxyFactory)
        {
            _router = proxyFactory.CreateHttp<IMeetingAccess>(ServiceUrl);
        }

        public FindResponse Find(FindRequest request)
        {
            return (FindResponse)_router.Handle(nameof(Find), request);
        }

        public ArrangeResponse Arrange(ArrangeRequest request)
        {
            return (ArrangeResponse)_router.Handle(nameof(Arrange), request);
        }

        public ProvisionResponse Provision(ProvisionRequest request)
        {
            return (ProvisionResponse)_router.Handle(nameof(Provision), request);
        }

        public ReportResponse Report(ReportRequest request)
        {
            return (ReportResponse) _router.Handle(nameof(Report), request);
        }
    }
}
