﻿using ConnectLabs.Connect.Interface.Trunking;
using ConnectLabs.Presence.Access.Contract;
using System;

namespace ConnectLabs.Presence.Access.Proxy.Http
{
    public class PresenceAccess : IPresenceAccess
    {
        private readonly IHttpProxy _router;

        private const string ServiceUrl = @"http://localhost:19081/Presence.Access/WebApi/api/Presence/";

        public PresenceAccess(IProxyFactory proxyFactory)
        {
            _router = proxyFactory.CreateHttp<IPresenceAccess>(ServiceUrl);
        }
        public FindResponse Find(FindRequest request)
        {
            return (FindResponse)_router.Handle(nameof(Find), request);
        }
    }
}
