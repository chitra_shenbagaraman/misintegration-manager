﻿using ConnectLabs.Connect.Interface.Trunking;
using ConnectLabs.MIS.Access.Contract;

namespace ConnectLabs.MIS.Access.Proxy
{
    public class MISAccess : IMISAccess
    {
        private readonly IHttpProxy _router;

        private const string ServiceUrl = @"http://localhost:19081/MIS.Access/WebApi/api/MIS/";

        public MISAccess(IProxyFactory proxyFactory)
        {
            _router = proxyFactory.CreateHttp<IMISAccess>(ServiceUrl);
        }

        public ViewResponse View(ViewRequest request)
        {
            return (ViewResponse) _router.Handle(nameof(View), request);
        }

        public MaintainResponse Maintain(MaintainRequest request)
        {
            return (MaintainResponse) _router.Handle(nameof(Maintain), request);
        }
    }
}
