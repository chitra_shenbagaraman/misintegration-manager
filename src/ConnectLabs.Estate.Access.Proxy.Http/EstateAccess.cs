﻿using ConnectLabs.Connect.Interface.Trunking;
using ConnectLabs.Estate.Access.Contract;

namespace ConnectLabs.Estate.Access.Proxy
{
    public class EstateAccess : IEstateAccess
    {
        private readonly IHttpProxy _router;

        private const string ServiceUrl = @"http://localhost:19081/Estate.Access/WebApi/api/Estate/";

        public EstateAccess(IProxyFactory proxyFactory)
        {
            _router = proxyFactory.CreateHttp<IEstateAccess>(ServiceUrl);
        }

        public ViewResponse View(ViewRequest request)
        {
            return (ViewResponse) _router.Handle(nameof(View), request);
        }

        public MaintainResponse Maintain(MaintainRequest request)
        {
            return (MaintainResponse) _router.Handle(nameof(Maintain), request);
        }

        public ProvisionResponse Provision(ProvisionRequest request)
        {
            return (ProvisionResponse)_router.Handle(nameof(Provision), request);
        }
    }
}
