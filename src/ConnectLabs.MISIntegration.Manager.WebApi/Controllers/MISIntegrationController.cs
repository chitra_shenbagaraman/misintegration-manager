﻿using ConnectLabs.Connect.Interface.Trunking;
using ConnectLabs.MISIntegration.Manager.Service.Contract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConnectLabs.MISIntegration.Manager.WebApi.Controllers
{
    public class MISIntegrationController : Controller, IMISIntegrationManager
    {
        private readonly IRequestRouter _router;

        public MISIntegrationController(IRequestRouter router)
        {
            _router = router ?? throw new ArgumentNullException(nameof(router));
        }
        public MigrateResponse Migrate(MigrateRequest request)
        {
            return (MigrateResponse)_router.Handle(nameof(Migrate), request);
        }
    }
}
