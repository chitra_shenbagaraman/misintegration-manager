﻿
using ConnectLabs.Connect.Mvc;
using ConnectLabs.Estate.Access.Contract;
using ConnectLabs.Estate.Access.Proxy;
using ConnectLabs.Meeting.Access.Contract;
using ConnectLabs.Meeting.Access.Proxy;
using ConnectLabs.MIS.Access.Contract;
using ConnectLabs.MIS.Access.Proxy;
using ConnectLabs.MISIntegration.Manager.Service.Contract;
using ConnectLabs.Presence.Access.Contract;
using ConnectLabs.Presence.Access.Proxy.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;


namespace ConnectLabs.MISIntegration.Manager.WebApi
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)

                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // Add Connect Mvc pipeline
            services.AddConnectMvc<IMISIntegrationManager>(new ConnectMvcSettings
            {
                provideDefinition = true
            });

            // Add connect services
            services.AddConnectService<IMISIntegrationManager>();

            // Add proxies
            services.AddConnectProxy<IEstateAccess, EstateAccess>();
            services.AddConnectProxy<IMISAccess, MISAccess>();
            services.AddConnectProxy<IMeetingAccess, MeetingAccess>();
            services.AddConnectProxy<IPresenceAccess, PresenceAccess>();

            //services.AddConnectProxy<IIdentityManager, IdentityManager>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseConnectMvc(builder =>
            {
                builder.UseCors()
                       .UseAuthentication(values =>
                       {
                           values.Token.Audience = "RoomBookingWebsite";
                           values.Token.Expires = true;
                       });
            });
        }

        /*
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello World!");
            });
        }
        */
    }
}
